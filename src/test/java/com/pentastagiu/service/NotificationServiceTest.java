package com.pentastagiu.service;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.concurrent.ExecutorService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.pentastagiu.email.EmailService;
import com.pentastagiu.models.Notification;
import com.pentastagiu.repository.NotificationRepository;
import com.pentastagiu.utils.enums.NotificationType;

@RunWith(SpringRunner.class)
public class NotificationServiceTest {

	@Mock
	NotificationRepository notificationRepository;

	@Mock
	EmailService emailService;

	@Mock
	ExecutorService executor;

	@InjectMocks
	NotificationService notificationService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSaveAndSendForInstantSend() {
		Notification notification = new Notification();
		notification.setNotificationType(NotificationType.PASSWORD_RESET);
		when(notificationRepository.save(Mockito.any())).thenReturn(null);

		notificationService.saveAndSend(notification);
		verify(notificationRepository, times(1)).save(notification);
	}

	@Test
	public void testSaveAndSendForScheduledSend() {
		Notification notification = new Notification();
		notification.setNotificationType(NotificationType.FINISHED_ORDER);
		when(notificationRepository.save(Mockito.any())).thenReturn(null);

		notificationService.saveAndSend(notification);
		verify(notificationRepository, times(1)).save(notification);
	}

	@Test(expected = Exception.class)
	public void testSendInstantMailThrowsException() {

		doThrow(new Exception()).when(emailService).sendSimpleMessage(Mockito.any(), Mockito.any(), Mockito.any());
		when(notificationRepository.save(Mockito.any())).thenReturn(null);
		notificationService.sendInstantMail(new Notification());

		verify(executor, times(1)).execute(Mockito.any());
	}

	@Test
	public void testSendInstantMailWhenMailIsSent() {
		when(notificationRepository.save(Mockito.any())).thenReturn(null);
		notificationService.sendInstantMail(new Notification());

		verify(executor, times(1)).execute(Mockito.any());
	}

	@Test(expected = Exception.class)
	public void testSendMailThrowsException() {
		doThrow(new Exception()).when(emailService).sendSimpleMessage(Mockito.any(), Mockito.any(), Mockito.any());
		when(notificationRepository.save(Mockito.any())).thenReturn(null);
		notificationService.sendMail(new Notification());
		verify(notificationRepository, times(1)).save(Mockito.any());
	}

	@Test
	public void testSendMailDoesNotThrowException() {
		when(notificationRepository.save(Mockito.any())).thenReturn(null);
		Notification not = new Notification("ceva@ceva", "asd", NotificationType.ACCOUNT_CREATED);
		notificationService.sendMail(not);
		verify(notificationRepository, times(1)).save(Mockito.any());
		verify(emailService, times(1)).sendSimpleMessage(Mockito.any(), Mockito.any(), Mockito.any());
	}

}
