package com.pentastagiu.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.pentastagiu.models.Notification;
import com.pentastagiu.repository.NotificationRepository;

@RunWith(SpringRunner.class)
public class ScheduledEmailSenderServiceTest {

	@Mock
	NotificationRepository notificationRepository;

	@Mock
	NotificationService notificationService;

	@InjectMocks
	ScheduledEmailSenderService scheduledEmailSenderService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSendEmailsAtFixedRate() {
		List<Notification> notifications = new ArrayList<Notification>();
		notifications.add(new Notification());
		notifications.add(new Notification());

		when(notificationRepository.findByNotificationStatusAndNotificationTypeIn(Mockito.any(), Mockito.any()))
				.thenReturn(notifications);

		scheduledEmailSenderService.sendEmailsAtFixedRate();

		verify(notificationRepository, times(1)).findByNotificationStatusAndNotificationTypeIn(Mockito.any(),
				Mockito.any());
	}
}
