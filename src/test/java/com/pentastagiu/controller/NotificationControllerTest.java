package com.pentastagiu.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.pentastagiu.email.EmailService;
import com.pentastagiu.models.Notification;
import com.pentastagiu.service.NotificationService;
import com.pentastagiu.utils.enums.NotificationType;

@RunWith(SpringRunner.class)
@WebAppConfiguration
public class NotificationControllerTest {

	@Mock
	NotificationService notificationService;

	@Mock
	EmailService emailService;

	private MockMvc mockMvc;

	@InjectMocks
	NotificationController notificationController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(notificationController).build();
	}

	@Test
	public void testAddNotificationShouldReturnOk() throws Exception {

		mockMvc.perform(post("/notification").contentType(MediaType.APPLICATION_JSON).param("email", "ceva@gmail.com")
				.param("last_name", "bogdan").param("first_name", "vacariuc")
				.param("type", NotificationType.ACCOUNT_CREATED.toString())).andExpect(status().isOk());

		Notification notification = new Notification("ceva@gmail.com",
				EmailService.createMessage("bogdan", "vacariuc", NotificationType.ACCOUNT_CREATED),
				NotificationType.ACCOUNT_CREATED);

		verify(notificationService, times(1)).saveAndSend(notification);
	}

}
