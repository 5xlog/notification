package com.pentastagiu.utils;

import com.pentastagiu.utils.enums.Status;

/**
 * Class used in the response that holds informations about the eventual errors
 * that occurred. A response can contain more that one ResponseStatus.
 * 
 * @author Vacariuc Bogdan
 *
 */
public class ResponseStatus {
	private String message;
	private Status errorStatus;

	public ResponseStatus(String message, Status errorStatus) {
		this.message = message;
		this.errorStatus = errorStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Status getErrorStatus() {
		return errorStatus;
	}

	public void setErrorStatus(Status errorStatus) {
		this.errorStatus = errorStatus;
	}
}
