package com.pentastagiu.utils.enums;

/**
 * Used in notification table to see if the email was sent or not
 * 
 * @author Vacariuc Bogdan
 *
 */
public enum NotificationStatus {
	SENT, NOT_SENT
}
