package com.pentastagiu.utils.enums;

/**
 * Used in notification table to see what is the type of the email.
 * 
 * @author Vacariuc Bogdan
 *
 */
public enum NotificationType {
	PASSWORD_RESET, ACCOUNT_CREATED, PLACED_ORDER, FINISHED_ORDER, NEWSLETTER
}
