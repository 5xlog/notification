package com.pentastagiu.utils.enums;

/**
 * Used in response to see if the request could be processed or not.
 * 
 * @author Vacariuc Bogdan
 *
 */
public enum Status {
	ERROR, OK
}
