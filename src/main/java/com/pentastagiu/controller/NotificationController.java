package com.pentastagiu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pentastagiu.email.EmailService;
import com.pentastagiu.models.Notification;
import com.pentastagiu.service.NotificationService;
import com.pentastagiu.utils.Response;
import com.pentastagiu.utils.ResponseStatus;
import com.pentastagiu.utils.enums.NotificationType;
import com.pentastagiu.utils.enums.Status;

/**
 * Controller class for services using notifications
 * 
 * @author Vacariuc Bogdan
 *
 */

@RestController
@RequestMapping("/notification")
public class NotificationController {

	@Autowired
	NotificationService notificationService;

	/**
	 * Creates a notification and saves it into the database, then, depending on its
	 * type, sends it directly, using an asynchronous thread or waits for a schedule
	 * job to send it at a rate specified in the properties file.
	 * 
	 * @param email
	 * @param lastName
	 * @param firstName
	 * @param type
	 * @return returns a ResponseEntity with the status set to OK and the body
	 *         containing a Response with the status OK and a message. The data
	 *         filed should be null
	 */
	@PostMapping(produces = "application/json")
	public ResponseEntity<?> createNotification(@RequestParam(value = "email") String email,
			@RequestParam(value = "last_name") String lastName, @RequestParam(value = "first_name") String firstName,
			@RequestParam(value = "type") NotificationType type) {

		Notification notification = new Notification(email, EmailService.createMessage(lastName, firstName, type),
				type);
		notificationService.saveAndSend(notification);

		ResponseStatus responseStatus = new ResponseStatus("Notification created", Status.OK);
		Response<Notification> response = new Response<Notification>(null, responseStatus);

		return ResponseEntity.ok(response);
	}
}
