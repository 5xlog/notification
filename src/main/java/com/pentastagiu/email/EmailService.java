package com.pentastagiu.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.pentastagiu.utils.enums.NotificationType;

/**
 * Service class for sending and creating mails.
 * 
 * @author Vacariuc Bogdan
 *
 */
@Component
public class EmailService {

	@Autowired
	public JavaMailSender emailSender;

	/**
	 * Sends the simple message using JavaMailSender object
	 * 
	 * @param to
	 * @param subject
	 * @param text
	 * @see JavaMailSender
	 */

	public void sendSimpleMessage(String receiver, String subject, String text) {
			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(receiver);
			message.setSubject(subject);
			message.setText(text);

			emailSender.send(message);
	}

	/**
	 * Creates a message for the specified notification
	 * 
	 * @param lastName
	 * @param firstName
	 * @param notificationType
	 * @return the message that has to be sent
	 */
	public static String createMessage(String lastName, String firstName, NotificationType notificationType) {
		return new String(lastName + " " + firstName + " " + notificationType.toString());
	}

}
