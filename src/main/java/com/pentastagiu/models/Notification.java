package com.pentastagiu.models;

import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.pentastagiu.utils.enums.NotificationStatus;
import com.pentastagiu.utils.enums.NotificationType;

/**
 * Model class for notification
 * 
 * @author Vacariuc Bogdan
 *
 */
@Entity
@Table(name = "notification")
public class Notification {

	@Id
	@Column(name = "notification_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long notificationId;

	@Column(name = "email")
	private String email;

	@Column(name = "text")
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private String text;

	@Column(name = "notification_status")
	private NotificationStatus notificationStatus;

	@Column(name = "type")
	private NotificationType notificationType;

	@Column(name = "created_time")
	private LocalDateTime createdTime;

	@Column(name = "sent_time")
	private LocalDateTime sentTime;

	public Notification() {
		notificationStatus = NotificationStatus.NOT_SENT;
	}

	public Notification(String email, String text, NotificationType notificationType) {
		notificationStatus = NotificationStatus.NOT_SENT;
		this.email = email;
		this.text = text;
		this.notificationType = notificationType;
	}

	@PrePersist
	public void onPrePersist() {
		createdTime = LocalDateTime.now();
	}

	@PreUpdate
	public void onPreUpdate() {
		sentTime = LocalDateTime.now();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public NotificationStatus getNotificationStatus() {
		return notificationStatus;
	}

	public void setNotificationStatus(NotificationStatus notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public LocalDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(LocalDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public LocalDateTime getSentTime() {
		return sentTime;
	}

	public void setSentTime(LocalDateTime sentTime) {
		this.sentTime = sentTime;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Notification other = (Notification) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (notificationStatus != other.notificationStatus)
			return false;
		if (notificationType != other.notificationType)
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
}
