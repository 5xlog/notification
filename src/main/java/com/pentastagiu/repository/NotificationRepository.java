package com.pentastagiu.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pentastagiu.models.Notification;
import com.pentastagiu.utils.enums.NotificationStatus;
import com.pentastagiu.utils.enums.NotificationType;

/**
 * Repository that contains queries for the notification table
 * 
 * @author Vacariuc Bogdan
 *
 */
@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {

	List<Notification> findByNotificationStatusAndNotificationTypeIn(NotificationStatus status,
			List<NotificationType> types);
}
