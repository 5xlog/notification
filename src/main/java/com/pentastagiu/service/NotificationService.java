package com.pentastagiu.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;

import com.pentastagiu.email.EmailService;
import com.pentastagiu.models.Notification;
import com.pentastagiu.repository.NotificationRepository;
import com.pentastagiu.utils.enums.NotificationStatus;
import com.pentastagiu.utils.enums.NotificationType;

/**
 * Service class for operations with notifications
 * 
 * @author Vacariuc Bogdan
 *
 */
@Service
public class NotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);

	private NotificationRepository notificationRepository;
	private EmailService emailService;
	private ExecutorService executor;

	@Autowired
	public NotificationService(NotificationRepository notificationRepository, EmailService emailService) {
		this.notificationRepository = notificationRepository;
		this.emailService = emailService;
		executor = Executors.newFixedThreadPool(5);
	}

	/**
	 * Checks if the received notification has to be sent immediately or not and
	 * saves it into the database or redirects it.
	 * 
	 * @param notification
	 */
	public void saveAndSend(Notification notification) {
		if (notification.getNotificationType().equals(NotificationType.PASSWORD_RESET)
				|| notification.getNotificationType().equals(NotificationType.ACCOUNT_CREATED)) {
			notificationRepository.save(notification);
			sendInstantMail(notification);

		} else if (notification.getNotificationType().equals(NotificationType.FINISHED_ORDER)
				|| notification.getNotificationType().equals(NotificationType.PLACED_ORDER)) {
			notificationRepository.save(notification);
		}
	}

	/**
	 * Creates an asynchronous thread and executes it into a fixed thread pool to
	 * send the email.
	 * 
	 * @param notification
	 */
	public void sendInstantMail(Notification notification) {
		Thread thread = new Thread(() -> {
			sendMail(notification);
		});
		executor.execute(thread);
	}

	/**
	 * Sends the email using EmailService and checks for exceptions that can occur.
	 * 
	 * @param notification
	 * @see JavaMailService
	 */
	public void sendMail(Notification notification) {
		try {
			emailService.sendSimpleMessage(notification.getEmail(), notification.getNotificationType().toString(),
					notification.getText());
			notification.setNotificationStatus(NotificationStatus.SENT);
			notificationRepository.save(notification);
		} catch (MailException e) {
			LOGGER.debug("Instant email could not be sent. MailSender threw exception.", e, e.getMessage());
		} catch (Exception e) {
			LOGGER.debug("Instant email could not be sent");
		}
	}

}
