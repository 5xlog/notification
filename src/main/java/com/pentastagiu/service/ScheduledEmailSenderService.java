package com.pentastagiu.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.pentastagiu.email.EmailService;
import com.pentastagiu.models.Notification;
import com.pentastagiu.repository.NotificationRepository;
import com.pentastagiu.utils.enums.NotificationStatus;
import com.pentastagiu.utils.enums.NotificationType;

@Service
public class ScheduledEmailSenderService {

	private NotificationRepository notificationRepository;
	private NotificationService notificationService;
	
	@Autowired
	public ScheduledEmailSenderService(NotificationRepository notificationRepository, EmailService emailService, NotificationService notificationService) {
		this.notificationRepository = notificationRepository;
		this.notificationService = notificationService;
	}
	
	/**
	 * Scheduled job that sends the mails that have NOT_SENT status and do not have
	 * to be sent instantly. The rate is set in the application.yml file
	 * 
	 */
	@Scheduled(fixedDelayString = "${emailrate}")
	public void sendEmailsAtFixedRate() {
		List<Notification> notifications = notificationRepository.findByNotificationStatusAndNotificationTypeIn(
				NotificationStatus.NOT_SENT,
				Arrays.asList(NotificationType.PLACED_ORDER, NotificationType.FINISHED_ORDER));
		for (Notification notification : notifications) {
			notificationService.sendMail(notification);
		}
	}
}
